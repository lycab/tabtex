#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
# Copyright 2013 Lycab
# 
# Permission to use, copy, modify and distribute this software for any purpose
# is hereby granted without fee, provided that the above copyright notice and 
# this permission notice appears in all copies and that the name of the
# copyright holder not be used in advertising or publicity pertaining to
# distribution of this software without specific, written prior permission. 
# Any distribution of this software or modifications of this software must be 
# free of charge. The copyright holder makes no representations about the 
# suitability of this software for any purpose. It is provided "as is" without
# express or implied warranty. The copyright holder is by no means responsible 
# for any damage taken by the  use of this software. It is absolutely on the 
# users own risk.
"""

import sys, getopt
def readin(dataFile, dstFile, delimChar="	") :
	dataArr=[]
	data = open(dataFile)
	for line in data:
		line=line.strip()
		dataArr.append(line.split(delimChar))
	return (dataArr, len(dataArr[0]), len(dataArr))

def div(offset, column, number, precision, dataFile, dstFile, delimChar="	"):
	column = column -1
	class data:
		dataArr = None
		x = 0
		y = 0
	
	dat = data()
	dat.dataArr=[]
	tmpArr=[]
	skipLines=[]
	count =0

	(dat.dataArr, dat.x, dat.y) = readin(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)

	for line in dat.dataArr:
		if count < offset:
			count = count + 1
			continue
		for i in range(len(line)):
			if i == column:
				if precision != -1:
					dat.dataArr[count][i]=str(round(float(line[i])/float(number), precision))
				else:
					dat.dataArr[count][i]=str(float(line[i])/float(number))
		count = count +1

	data = open(dstFile, "w")
	count = 0
	for row in skipLines:
		data.write(row)

	for row in dat.dataArr:
		for column in row:
			if count != len(row)-1:
				count = count +1
				data.write(column + delimChar)
			else:
				data.write(column)
				count =0
		data.write("\n")
	data.close()




def rnd(dataFile, dstFile, column, precision, offset, delimChar="	") :
	column = column - 1
	class data:
		dataArr = None
		x = 0
		y = 0
	
	dat = data()
	dat.dataArr=[]
	tmpArr=[]
	skipLines=[]
	count =0

	(dat.dataArr, dat.x, dat.y) = readin(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)

	for line in dat.dataArr:
		if count < offset:
			count = count + 1
			continue
		for i in range(len(line)):
			if i == column:
				dat.dataArr[count][i]=str(round(float(line[i]),precision))
		count = count +1

	data = open(dstFile, "w")
	count = 0
	for row in skipLines:
		data.write(row)

	for row in dat.dataArr:
		for column in row:
			if count != len(row)-1:
				count = count +1
				data.write(column + delimChar)
			else:
				data.write(column)
				count =0
		data.write("\n")
	data.close()

def maketab(dataFile, dstFile, delimChar, caption, label, german=False, longtable=False) :
	class data:
		dataArr=None
		x=0
		y=0

	dat=data()
	dat.dataArr=[]
	(dat.dataArr, dat.x, dat.y) = readin(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)
	dstData = open(dstFile, "w")
	if not longtable:
		if caption != "":
			dstData.write("\\begin{table}[H]\n\\begin{center}\n\\caption{" + caption + "}\n\\label{" + label + "}\n\\begin{tabular}{" + "r" * dat.x + "}\n")
		else:
			dstData.write("\\begin{table}[H]\n\\begin{center}\n\\caption{}%TODO\n\\label{" + label + "}\n\\begin{tabular}{" + "r" * dat.x + "}\n")
	else:
		if caption != "":
			dstData.write("\\begin{center}\n\\begin{longtable}{" + "r" * dat.x + "}\n\\caption{" + caption + "}\n\\label{" + label + "}\n")
		else:
			dstData.write("\\begin{center}\n\\begin{longtable}{" + "r" * dat.x + "}\n\\caption{}%TODO\n\\label{" + label + "}\n")
	countItems = 0 
	countLines = 0
	if not longtable:
		dstData.write("\\toprule\n")
	for line in dat.dataArr:
		if countLines == 1:
			if not longtable: 
				dstData.write("\\midrule\n")
			else:
				dstData.write("\\hline\n\\endfirsthead\n\\endhead\n\\endfoot\n\\hline\n\\endlastfoot\n")
		for item in line:
			if countItems !=len(line)-1:
				countItems = countItems + 1
				if german:
					dstData.write(item.replace('.', ',') + " & ")
				else:
					dstData.write(item + " & ")
			else:
				if german:
					dstData.write(item.replace('.', ',') + "\\\\")
				else:
					dstData.write(item + "\\\\")
				countItems = 0
		dstData.write("\n")
		countLines = countLines + 1
		if countLines == dat.y:
			if not longtable:
				dstData.write("\\bottomrule\n")
	if not longtable:
		dstData.write("\\end{tabular}\n\\end{center}\n\\end{table}\n")
	else:
		dstData.write("\\end{longtable}\n\\end{center}\n")
	dstData.close()

def printHelp():
	print "Usage: tabtex.py [OPTIONS]"
	print "Convert CSV-like-File to Latex Table"
	print "File has to be in format: [Text/Data][delimiter][Text/Data]... \n"
	print "Example: tabtex.py -i aufgabe2.data -o aufgabe2.tab.tex\n"
	print "Options:"
	print "  -h, --help\t\t Print this help"
	print "  -i, --input\t\t Input File"
	print "  -o, --output\t\t Output File"
	print "  -d, --delimiter\t Delimiter (Must be one of [' ', 't', ';', ','])"
	print "  -C, --caption\t\t Caption of Table"
	print "  -l, --label\t\t Latex Label of Table"
	print "  -n, --no-round\t Disable rounding"
	print "  -N, --no-german\t Disable substitution of '.' with ','"
	print "  -I, --in-place\t Save to Input File (will nevertheless create and delete temporary files)"
	print "                \t will override -o"
	print "  -c, --config\t\t Use Config File. If no file submittet, the standard File ~/.tabtex will be used if possible"
	print "  -p --longtable\t Use Longtable package for long tables over multiple pages"
	print "  -k --divide\t\t Enable asking if one column is to be divided by a number (eg 1000, 1e6 etc)"

def readConfig(configFile):
	delimChar = ""
	rndFlag = "n"
	inplace = False
	german = True
	easyOut = False

	if configFile == "":
		configFile="~/.tabtex"
	try:

		with open(configFile) as data:
			conf = dict(line.strip().split(None,1) for line in data)
	except:
		print "Error with config file. Either no File or wrong format"
		sys.exit(3)
	try:
		delimChar = conf["delimChar"]
	except:
		pass

	try:
		rndFlag = conf["rndFlag"]
	except:
		pass

	try:
		german = "True" == conf["german"]
	except:
		pass

	try:
		inplace = "True" == conf["inplace"]
	except:
		pass
	try:
		easyOut = "True" == conf["easyOut"]
	except:
		pass

	if delimChar == "t":
		delimChar = "	"
	
	return (delimChar, rndFlag, inplace, german, easyOut)



#-----------------------------------------------------------------------#
#-----------------------Executed----------Code--------------------------#
#-----------------------------------------------------------------------#
print "****************************************"
print "TabTex - Version 1.2.1 [Jan 29 2014]	"
print "****************************************"
print "Using Python Version:"
print sys.version
print "****************************************"
print

german=True
easyOut = False
if len(sys.argv) > 1:
	opts, args = getopt.getopt(sys.argv[1:], "c:hi:o:d:nNIeC:l:pk", ["help", "input=", "output=", "delimiter=", "no-round", "no-german", "in-place", "easyout", "config", "caption", "label", "longtable", "divide"])
	dataFile=""
	dstFile=""
	delimChar=""
	rndFlag=""
	caption=""
	label=""
	inplace = False
	longtable = False
	divide = False

	for opt, arg in opts:
		if opt in {"-i", "--input"}:
			dataFile = arg
		elif opt in {"-c", "--config"}:
			(delimChar, rndFlag, inplace, german, easyOut) = readConfig(arg)
			break
		elif opt in {"-o", "--output"}:
			dstFile = arg
		elif opt in {"-d", "--delimiter"}:
			if arg in {" ", ";", ","}:
				delimChar = arg
			elif arg == "t":
				delimChar = "	"
			else:
				print "ERROR"
				print "Delimiter in wrong format for commandline Option, for any delimiter use input in program\n"
				printHelp()
				sys.exit(3)

		elif opt in {"-h", "--help"}:
			printHelp()
			sys.exit(1)
		elif opt in {"-n", "--no-round"}:
			rndFlag = "n"
		elif opt in {"-N", "--no-german"}:
			german = False
		elif opt in {"-I", "--in-place"}:
			dstFile = dataFile
			inplace = True
		elif opt in {"-e", "--easyout"}:
			easyOut = True
		elif opt in {"-C", "--caption"}:
			caption = arg
		elif opt in {"-l", "--label"}:
			label = arg
		elif opt in {"-p", "--longtable"}:
			longtable = True
		elif opt in {"-k", "--divide"}:
			divide = True

	if dataFile == "":
		dataFile = raw_input("Input File: ")
	if dstFile == "":
		if not inplace:
			dstFile = raw_input("Output File: ")
		else:
			dstFile=dataFile
	if delimChar == "":
		delimChar = raw_input("Delimiter: ")
	if caption == "":
		caption = raw_input("Caption: ")
	if label == "":
		label = raw_input("Label: ")
#	if rndFlag == "":
#		rndFlag = raw_input("Round? [y/N] ")


else: 
	dataFile = raw_input("Input File: ")
	dstFile = raw_input("Output File: ")
	delimChar = raw_input("Delimiter: ")
	caption = raw_input("Caption: ")
	label = raw_input("Label: ")
	rndFlag = raw_input("Round? [y/N] ")

if easyOut:
	dstFile = dataFile.split(".")[0] + ".tab.tex"

if divide:
	print "Dividing"
	offset = int(raw_input("Offset "))
	column = int(raw_input("Column "))
	number = int(raw_input("Divide by "))
	precision = int(raw_input("Round to Precision (-1: full) "))
	while True:
		div(offset, column, number, precision, dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)
		dataFile=dstFile
		print "Done dividing, insert column=0 for end of dividing"
		column = int(raw_input("Column "))
		if column == 0:
			break
		number = int(raw_input("Divide by "))
		precision = int(raw_input("Round to Precision (-1: full) "))

if rndFlag == "":
	rndFlag = raw_input("Round? [y/N] ")

if rndFlag in {"y", "ye", "yes", "Y", "Ye", "Yes"}:
	print "Rounding"
	offset = int(raw_input("Offset "))
	column = int(raw_input("Column "))
	precision = int(raw_input("Precision "))
	while True:
		rnd(dataFile=dataFile, dstFile=dstFile, column=column, precision=precision, offset=offset, delimChar=delimChar)
		dataFile=dstFile
		print "Done rounding, insert column=0 for end of rounding"
		column = int(raw_input("Column "))
		if column == 0:
			break
		precision = int(raw_input("Precision "))
	dataFile=dstFile
	maketab(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar, german=german, caption=caption, label=label, longtable=longtable)
	print "Done"

elif rndFlag in {"n", "no", "N", "No", ""}:
	maketab(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar, german=german, caption=caption, label=label, longtable=longtable)
	print "Done"

else:
	print "No Valid Answer"

