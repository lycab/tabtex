#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
# Copyright 2013 Lycab
# 
# Permission to use, copy, modify and distribute this software for any purpose
# is hereby granted without fee, provided that the above copyright notice and 
# this permission notice appears in all copies and that the name of the
# copyright holder not be used in advertising or publicity pertaining to
# distribution of this software without specific, written prior permission. 
# Any distribution of this software or modifications of this software must be 
# free of charge. The copyright holder makes no representations about the 
# suitability of this software for any purpose. It is provided "as is" without
# express or implied warranty. The copyright holder is by no means responsible 
# for any damage taken by the  use of this software. It is absolutely on the 
# users own risk.
"""

from ttk import *
from Tkinter import *
import tkFileDialog
from os import remove

class App:
	def __init__(self, master):
		master.title("TabTex v1.0-GUI")

		self.style = Style()
		self.style.theme_use("clam")

		frame = Frame(master, relief = RAISED, borderwidth=1, padx=15, pady=15)
		frame.pack(fill=BOTH, expand =1)

		self.german=IntVar()
		self.inputFileName=""
		self.outputFileName=""
		self.font=('sansserif', 10, 'normal')

		self.master = master
		self.create(frame)
		self.fill(frame)
	
	def create(self, frame):
		self.lHeader = Label(frame, text="Files:")
		self.lHeader.config(font=('sansserif', 15, 'bold'))
		self.lOptions = Label(frame, text="Options:", font=('sansserif', 15, 'bold'))
		self.lInputFileName = Label(frame, text="No File Selected", font=self.font)
		self.lOutputFileName = Label(frame, text = "No File Selected", font=self.font)
		self.bInputFileName = Button(frame, text="Input File", command=self.getinputname, font=self.font)
		self.bOutputFileName = Button(frame, text="Output File", command=self.getoutputname, font=self.font)
		self.lDelimChar = Label(frame, text="Delimiter:", font=self.font)
		self.iDelimChar = Entry(frame)
		self.lDelimCharTip = Label(frame, text="\tt for {TAB}", font=('sansserif', 8, 'italic'))
		self.lCaption = Label(frame, text="Table Caption:", font=self.font)
		self.iCaption = Entry(frame)
		self.lLabel = Label(frame, text="Table Label:", font=self.font)
		self.iLabel = Entry(frame)
		self.cGerman = Checkbutton(frame, text="Replace '.' with ','", variable=self.german, font=self.font)
		self.cGerman.select()
		self.fButtons=Frame(self.master, borderwidth=1, padx=10)

		self.bRound = Button(self.fButtons, text="Round", command=self.rndDialog, font=self.font)
		self.lSuc = Label(self.fButtons, text="", fg="red", font=self.font)

		self.bRun = Button(self.fButtons, text="Create Table", command=self.Run, font=self.font)
		self.bClose=Button(self.fButtons, text="Quit", command=frame.quit, font=self.font)
	def setSpacer(self, frame, x, y, gridx, gridy):
		self.spacer = Label(frame, text=" ")
		self.spacer["width"]=x
		self.spacer["height"]=y
		self.spacer.grid(row=gridx, column=gridy)

	def fill(self, frame):
		self.lHeader.grid(sticky=W)
		self.lInputFileName.grid(sticky=W, columnspan=2)
		self.lOutputFileName.grid(sticky=W, columnspan=2)
		self.bInputFileName.grid(row=1, column =2, sticky=E)
		self.bOutputFileName.grid(row=2, column =2, sticky=E)
		self.setSpacer(frame, 1, 1, 3, 0)
		self.lOptions.grid(row=4, column=0, sticky=W)
		self.setSpacer(frame, 1, 1, 5, 0)
		self.lDelimChar.grid(row=6, column=0, sticky=W)
		self.iDelimChar.grid(row=6, column=1, sticky=W)
		self.lDelimCharTip.grid(row=6, column=2, sticky=E)
		self.lCaption.grid(row=7, column=0, sticky=W)
		self.iCaption.grid(row=7, column=1, sticky=W)
		self.lLabel.grid(row=8, column=0, sticky=W)
		self.iLabel.grid(row=8, column=1, sticky=W)
		self.setSpacer(frame, 1, 1, 9, 0)
		self.cGerman.grid(row=10, column=1, sticky=W)
		self.setSpacer(frame, 2, 2, 11, 0)

		self.fButtons.pack(fill=X)

		self.bRound.pack(side=LEFT)
		self.bRun.pack(side=LEFT)
		self.lSuc.pack(side=LEFT)
		self.bClose.pack(side=RIGHT)
		#self.bRound.grid(row=13, column=0, sticky=W)
		#self.bRun.grid(row=13, column=1, sticky=W)

	
		#self.setSpacer(frame, 3, 3, 12, 12)
		#self.lSuc.grid(row=13, column=2)
		#self.bClose.grid(row=13, column=11)
	def getinputname(self):
		self.inputFileName = tkFileDialog.askopenfilename()
		self.lInputFileName["text"] = self.inputFileName
	def getoutputname(self):
		self.outputFileName = tkFileDialog.asksaveasfilename()
		self.lOutputFileName["text"] = self.outputFileName
	def rndDialog(self):
		show=False
		if self.inputFileName!="" and self.outputFileName!="" and self.iDelimChar.get()!="":
			show=True
		else:
			self.Error = Toplevel(padx=15, pady=15)
			self.Error.title("Error")
			self.lError = Label(self.Error, text="Wrong Data Input", font=self.font)
			self.lError.pack()
			self.bQuit = Button(self.Error, text="Close", command=self.Error.destroy, font=self.font)
			self.bQuit.pack()
		if show:

			self.rndDT = Toplevel()
			self.rndDT.title("Round")

			self.rndD = Frame(self.rndDT, padx=15, pady=15)
			self.rndD.pack()


			self.lColumn = Label(self.rndD, text="Column:", font=self.font)
			self.lColumn.grid(row=1, column=0, sticky=W)
			self.iColumn = Entry(self.rndD)
			self.iColumn.grid(row=1, column=1, sticky=W)

			self.lPrecision= Label(self.rndD, text="Precision", font=self.font)
			self.lPrecision.grid(row=2, column=0, sticky=W)
			self.sPrecision = Scale(self.rndD, from_=1, to=6, orient=HORIZONTAL)
			self.sPrecision.grid(row=2, column=1, sticky=W)
			self.sPrecision.set(3)

			self.lOffset= Label(self.rndD, text="Offset", font=self.font)
			self.lOffset.grid(row=3, column=0, sticky=W)
			self.sOffset= Scale(self.rndD, from_=0, to=6, orient=HORIZONTAL)
			self.sOffset.grid(row=3, column=1, sticky=W)
			self.sOffset.set(1)

			self.frndButtons=Frame(self.rndDT, padx=15, pady=15)
			self.frndButtons.pack()
			
			self.bStart = Button(self.frndButtons, text = "Start", command=self.startRnd, font=self.font)
			#self.bStart.grid(row=4, column =0)
			self.bStart.pack(side=LEFT)

			self.lDone = Label(self.frndButtons,fg="red", text="", font=self.font)
			#self.lDone.grid(row=4, column=1)
			self.lDone.pack(side=LEFT)

			self.bQuitRnd = Button(self.frndButtons, text="Quit", command=self.rndDT.destroy, font=self.font)
			#self.bQuitRnd.grid(row=4, column =2)
			self.bQuitRnd.pack(side=RIGHT)
	def startRnd(self):
		noerror=True
		try:
			int(self.iColumn.get())
		except:
			noerror=False
			print 'ERROR'
		if noerror:
			rnd(str(self.inputFileName), str(self.outputFileName + ".round"), int(self.iColumn.get()), int(self.sPrecision.get()), int(self.sOffset.get()), str(self.iDelimChar.get()))
			self.inputFileName = self.outputFileName + ".round"
			self.lDone["text"]="Done"
	def Run(self):
		noerror=False
		if self.inputFileName != "" and self.outputFileName != "" and self.iDelimChar.get() != "":
			maketab(self.inputFileName, self.outputFileName, self.iDelimChar.get(), self.iCaption.get(), self.iLabel.get(), self.german)
			self.lSuc["text"]="Done"


def readin(dataFile, dstFile, delimChar="       ") :
	dataArr=[]
        data = open(dataFile, "r")
        for line in data:
                line=line.strip()
                dataArr.append(line.split(delimChar))
        return (dataArr, len(dataArr[0]), len(dataArr))
		

def rnd(dataFile, dstFile, column, precision, offset, delimChar="       ") :
        column = column - 1
	if delimChar=="t":
		delimChar="	"

        class data:
                dataArr = None
                x = 0
                y = 0

        dat = data()
        dat.dataArr=[]
        tmpArr=[]
        skipLines=[]
        count =0

        (dat.dataArr, dat.x, dat.y) = readin(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)

        for line in dat.dataArr:
                if count < offset:
                        count = count + 1
                        continue
                for i in range(len(line)):
                        if i == column:
                                dat.dataArr[count][i]=str(round(float(line[i]),precision))
                count = count +1

        data = open(dstFile, "w")
        count = 0
        for row in skipLines:
                data.write(row)

        for row in dat.dataArr:
                for column in row:
                        if count != len(row)-1:
                                count = count +1
                                data.write(column + delimChar)
                        else:
                                data.write(column)
                                count =0
                data.write("\n")
        data.close()
        
def maketab(dataFile, dstFile, delimChar, caption, label, german=False) :
	class data:
		dataArr=None
		x=0
		y=0
	if delimChar=="t":
		delimChar="	"

	dat=data()
	dat.dataArr=[]
	(dat.dataArr, dat.x, dat.y) = readin(dataFile=dataFile, dstFile=dstFile, delimChar=delimChar)
	dstData = open(dstFile, "w")
	if caption != "":
		dstData.write("\\begin{table}[H]\n\\begin{center}\n\\caption{" + caption + "}\n\\label{" + label + "}\n\\begin{tabular}{" + "r" * dat.x + "}\n")
	else:
		dstData.write("\\begin{table}[H]\n\\begin{center}\n\\caption{}%TODO\n\\label{" + label + "}\n\\begin{tabular}{" + "r" * dat.x + "}\n")
	countItems = 0 
	countLines = 0
	dstData.write("\\toprule\n")
	for line in dat.dataArr:
		if countLines == 1:
			dstData.write("\\midrule\n")
		for item in line:
			if countItems !=len(line)-1:
				countItems = countItems + 1
				if german:
					dstData.write(item.replace('.', ',') + " & ")
				else:
					dstData.write(item + " & ")
			else:
				if german:
					dstData.write(item.replace('.', ',') + "\\\\")
				else:
					dstData.write(item + "\\\\")
				countItems = 0
		dstData.write("\n")
		countLines = countLines + 1
		if countLines == dat.y:
			dstData.write("\\bottomrule\n")
	dstData.write("\\end{tabular}\n\\end{center}\n\\end{table}\n")
	dstData.close()
	remove(dstFile + ".round")
	
root = Tk()
app = App(root)
root.mainloop()

