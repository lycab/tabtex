#TabTex

TabTex is a python script to easily convert raw data tables to LaTeX tables.

There is a commandline version which should be used and there is a gui version
which might contain some bugs. 

The commandline version is easy-to-use. There are two ways to use it.

1. Interactive: Just execute it and "answer" the "questions"
2. Truly Commandline: example: ./tabtex.py -i infile -o outfile -n -d t -n
   For a complete list of options use ./tabtex.py --help



I know the code could be much nicer, but i'm too lazy to clean it up ;)

Anyway, if you find some bugs or have questions, feel free to tell me at
[Bitbucket/lycab/tabtex](Https://bitbucket.org/lycab/tabtex)
